<?php

namespace Shop\Service;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class Database
{
    private static $connection;
    private static $entityManager;

    private static $connectionParams = array(
        'dbname' => 'studyshop',
        'user' => 'studyshop',
        'password' => '1',
        'host' => '127.0.0.1:3306',
        'driver' => 'pdo_mysql',
    );

    public static function GetConnection()
    {


        if(null === static::$connection) {

            $config = new \Doctrine\DBAL\Configuration();


            static::$connection = \Doctrine\DBAL\DriverManager::getConnection(static::$connectionParams, $config);
        }
        return static::$connection;

    }

    /**
     * @return EntityManager
     */
    public static function GetEntityManager()
    {
        if(null===static::$entityManager) {
            $paths = array(DOCKROOT . "/Shop");
            $isDevMode = true;

            $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
            static::$entityManager = EntityManager::create(static::$connectionParams, $config);
        }


        return static::$entityManager;

    }
}