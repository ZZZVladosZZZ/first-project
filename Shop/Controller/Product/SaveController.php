<?php

namespace Shop\Controller\Product;

use Shop\Controller\ControllerInterface;
use Shop\Service\Database;
use Shop\Model\Product;
use Shop\Service\NotFoundException;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class SaveController implements ControllerInterface
{
    public function execute($request, $response)
    {
        try {
            $params = $request->paramsPost();

            if (!empty($params['id'])) {
                $product = Database::GetEntityManager()
                    ->getRepository(Product::class)
                    ->find($params['id']);
            } else {
                $product = new Product();
            }
            if (!$product) {
                throw new NotFoundException();
            }
            $product->setName($params['name']);
            $product->setDescription($params['description']);
            $product->setPrice($params['price']);
            if (empty($params['id'])) {
                Database::GetEntityManager()->persist($product);
            }
            Database::GetEntityManager()->flush();

            return $response->redirect("/product/view/" . $product->getId());
        } catch (NotFoundException $e) {
            $loader = new FilesystemLoader(DOCKROOT.'/Shop/View');
            $template = new Environment($loader);
            return  $template->render('noresults.html');

        } catch (\Exception $exception) {
            echo  $exception->getMessage();
        }

    }
}